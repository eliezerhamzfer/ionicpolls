# APLICACION ENCUESTAS HECHA EN IONIC FRAMEWORK #

Servidor hecho en python-django, para ver el repositorio del backend ir a [Django-Polls](https://bitbucket.org/eliezerhamzfer/djangopolls).

* En branch master está la versión para montarlo en localhost y consumir los datos del server de Django
* El branch beta lo uso para testing devel
* En branch ionic-standlone está la versión para testearlo sin conexion al servidor, con datos estaticos

### Requisitos para desarollo ###
* NodeJS>=0.12.3
* Cordova>=5.0.0
* Ionic>=1.5.2
* Andoid SDK