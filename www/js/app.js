// encuestapp App
angular.module('starter', ['ionic', 'starter.controllers', 'starter.services'])
.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider

  .state('app', {
    url: "/app",
    abstract: true,
    templateUrl: "templates/menu.html",
    controller: 'AppCtrl'
  })
  .state('app.encuestas', {
    url: "/encuestas",
    views: {
      'menuContent': {
        templateUrl: "templates/encuestas.html",
        controller: 'EncuestasController'
      }
    }
  })
  .state('app.encuesta', {
    url: "/encuesta/:encuestaID",
    views: {
      'menuContent': {
        templateUrl: "templates/encuesta.html",
        controller: 'EncuestaController'
      }
    }
  })
  .state('app.resultado', {
    url: "/resultado/:encuestaID",
    views: {
      'menuContent': {
        templateUrl: "templates/resultado.html",
        controller: 'ResultadoController'
      }
    }
  })
  .state('app.respuestas', {
    url: "/respuestas",
    views: {
      'menuContent': {
        templateUrl: "templates/respuestas.html",
        controller: 'RespuestasController'
      }
    }
  });
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/encuestas');
});
